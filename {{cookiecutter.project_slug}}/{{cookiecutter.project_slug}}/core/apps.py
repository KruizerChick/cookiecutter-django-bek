from django.apps import AppConfig

# from suit.apps import DjangoSuitConfig


class CoreConfig(AppConfig):
    name = '{{ cookiecutter.project_slug }}.core'
    label = 'core'

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        try:
            import core.signals  # noqa F401
        except ImportError:
            pass


# class SuitConfig(DjangoSuitConfig):
#     layout = 'horizontal'
