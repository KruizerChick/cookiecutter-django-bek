""" Misc Utilities """

from django.contrib.staticfiles.apps import StaticFilesConfig


class MyStaticFilesConfig(StaticFilesConfig):
    """ Extends ignore patterns for staticfile finder. """
    ignore_patterns = [
        '*/sass/*', '*.scss',
        '*/venv/*',
        '*/tinymce/static/*', '*/langs/*',
    ]
