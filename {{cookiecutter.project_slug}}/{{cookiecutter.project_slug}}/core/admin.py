
from django import forms
from django.contrib import admin

from django.contrib.flatpages.admin import FlatPageAdmin as BaseFlatPageAdmin
from django.contrib.flatpages.models import FlatPage
from django.db import models
from django.shortcuts import render, reverse
from django.utils.translation import gettext_lazy as _


# Register your models here.

# Customize Admin Site
admin.sites.AdminSite.site_header = '{{ cookiecutter.project_slug }} Administration'
admin.sites.AdminSite.site_title = '{{ cookiecutter.project_slug }} Administration'


class MyFlatPageAdmin(BaseFlatPageAdmin):
    """ Define a new FlatPageAdmin  """
    fieldsets = (
        (None, {'fields': ('url', 'title', 'content', 'sites')}),
        (_('Advanced options'), {
            'classes': ('collapse', ),
            'fields': (
                'enable_comments',
                'registration_required',
                'template_name',
            ),
        }),
    )
    save_on_top = True


# Re-register FlatPageAdmin
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, MyFlatPageAdmin)
