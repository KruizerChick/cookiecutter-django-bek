
import json
from django.contrib.admin import widgets
from django.contrib.staticfiles.storage import staticfiles_storage
from django.forms import Media
from django.utils.safestring import mark_safe

# from taggit.models import Tag

# from ..people.models import Person


# Widgets
class MiniTextarea(widgets.AdminTextareaWidget):
    """ Vertically shorter version of the admin textarea widget. """
    rows = 2

    def __init__(self, attrs=None):
        super(MiniTextarea, self).__init__(
            {'rows': self.rows})


class SmallTextarea(widgets.AdminTextareaWidget):
    """ Vertically shorter version of the admin textarea widget. """
    rows = 4

    def __init__(self, attrs=None):
        super(SmallTextarea, self).__init__(
            {'rows': self.rows})
