# Mixins
class OwnerMixin(object):
    """ Filters the queryset to the owner of the object """
    def get_queryset(self):
        qs = super(OwnerMixin, self).get_queryset()
        return qs.filter(owner=self.request.user)


class OwnerEditMixin(object):
    """ Validates the form and redirects to the 'success' URL """
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(OwnerEditMixin, self).form_valid(form)
